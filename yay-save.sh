#!/usr/bin/bash

yay -Qqe > /home/cody/Documents/Git/linux-setup-scripts/arch/manjaro-workstation/pkglist.txt
git -C /home/cody/Documents/Git/linux-setup-scripts add /home/cody/Documents/Git/linux-setup-scripts/arch/manjaro-workstation/pkglist.txt
git -C /home/cody/Documents/Git/linux-setup-scripts commit -m 'Update packages'
