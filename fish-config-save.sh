#!/usr/bin/bash

cp /home/cody/.config/fish/config.fish /home/cody/Documents/Git/linux-setup-scripts/arch/manjaro-workstation/config.fish
git -C /home/cody/Documents/Git/linux-setup-scripts add /home/cody/Documents/Git/linux-setup-scripts/arch/manjaro-workstation/config.fish
git -C /home/cody/Documents/Git/linux-setup-scripts commit -m 'Update fish config'
