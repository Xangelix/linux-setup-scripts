from os import walk
import os.path
import json, argparse
import requests
import time

parser = argparse.ArgumentParser()
parser.add_argument("server_folder")
args = parser.parse_args()

server = args.server_folder

channels = []
for (dirpath, dirnames, filenames) in walk(server):
    channels.extend(dirnames)
    break

for channel in channels:
    with open(server + channel + '/' + channel + '.json') as f:
        data = json.load(f)
    for message in data['messages']:
        if message['attachments'] != []:
            urls = message['attachments'][0]['url']
            for url in urls.splitlines():
                time.sleep(1)
                local_filename = server + channel + '/' + url.split('/')[-3] + '-' + url.split('/')[-2] + '-' + url.split('/')[-1]
                #mod = 0
                #while os.path.isfile(local_filename):
                #    if mod == 0:
                #        local_filename = local_filename[:local_filename.rindex('.')] + '()' + local_filename[local_filename.rindex('.'):]
                #    mod += 1
                #    local_filename = local_filename[:local_filename.rindex('(') + 1] + str(mod) + local_filename[local_filename.rindex(')'):]
                with requests.get(url, stream=True) as r:
                    r.raise_for_status()
                    with open(local_filename, 'wb') as f:
                        for chunk in r.iter_content(chunk_size=8192): 
                            f.write(chunk)
                print(local_filename)
