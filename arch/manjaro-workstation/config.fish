set -x CHROME_EXECUTABLE google-chrome-stable

starship init fish | source

alias xanup="/home/cody/Documents/Git/linux-setup-scripts/yay-save.sh; /home/cody/Documents/Git/linux-setup-scripts/fish-config-save.sh"
echo "fish.config loaded"
