#!/bin/sh
pandoc -o $1.pdf $1 --template=mla-template.tex --pdf-engine=xelatex

pandoc -s -f markdown -t odt -o $1.odt $1 --template=mla-template.tex --pdf-engine=xelatex